This project is no longer maintained.

The contents of this repository are still available in the Git
source code management system.  To see the contents of this
repository before it reached its end of life, please check out the
previous commit with ``git checkout HEAD^1``.

Historical content may still be viewed at
http://specs.openstack.org/openstack/swift-specs/
